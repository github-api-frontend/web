FROM node:15.5.0-alpine3.10 as builder

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .


RUN npm run build

FROM nginx:1.19.6-alpine

COPY --from=builder /app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]