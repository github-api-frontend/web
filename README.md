# Github API Frontend Web
This repository stores all the web client code for user interaction with the primary API service.
## Usage
This application can be compiled as a standalone static Vue.js site or deployed through Docker using the provided Dockerfile.

### Building a standalone static site
``npm run build``

### Building a Docker image 
``docker build -t registry.gitlab.com/github-api-frontend/web:custom .``

### Configuration
All configuration variables must be defined in `gaf.config.js` prior to compilation and deployment.