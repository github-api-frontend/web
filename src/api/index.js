import axios from 'axios';
import gafConfig from '../../gaf.config.js'

const createAxiosRequest = (route, data, method) => {
    let config = {url: gafConfig.ApiHost + route, data: data, method: method, withCredentials: true}

    return axios(config)
}

export const tryLogin = (username, password) => new Promise((resolve, reject) => {
    createAxiosRequest('/login', {username: username, password: password}, 'POST')
        .then(() => {
            resolve()
        })
        .catch(err => {
            reject(err)
        })
})

export const tryRegister = (username, password) => new Promise((resolve, reject) => {
    createAxiosRequest('/register', {username: username, password: password}, 'POST')
        .then(() => {
            resolve()
        })
        .catch(err => {
            reject(err)
        })
})

export const getUserInfo = () => new Promise((resolve, reject) => {
    createAxiosRequest('/user/info', {}, 'GET')
        .then(resp => {
            resolve(resp.data)
        })
        .catch(err => {
            reject(err)
        })
})

export const logout = () => new Promise((resolve, reject) => {
    createAxiosRequest('/logout', {}, 'POST')
        .then(() => {
            resolve()
        })
        .catch(err => {
            reject(err)
        })
})

export const getEvents = () => new Promise((resolve, reject) => {
    createAxiosRequest('/admin/events', {}, 'GET')
        .then(resp => {
            resolve(resp.data)
        })
        .catch(err => {
            reject(err)
        })
})

export const getUsers = () => new Promise((resolve, reject) => {
    createAxiosRequest('/admin/users', {}, 'GET')
        .then(resp => {
            resolve(resp.data)
        })
        .catch(err => {
            reject(err)
        })
})

export const getRepos = (query, page, page_size) => new Promise((resolve, reject) => {
    createAxiosRequest('/repo/search',
        {query: query, page: page, per_page: page_size}, 'POST')
        .then(resp => {
            resolve(resp.data)
        })
        .catch(err => {
            reject(err)
        })
})

export const getRepo = (repoId) => new Promise((resolve, reject) => {
    createAxiosRequest('/repo/' + repoId + '/info',
        {}, 'GET', false)
        .then(resp => {
            resolve(resp.data)
        })
        .catch(err => {
            reject(err)
        })
})

export const getFavorites = () => new Promise((resolve, reject) => {
    createAxiosRequest('/user/favorites',
        {}, 'GET')
        .then(resp => {
            resolve(resp.data)
        })
        .catch(err => {
            reject(err)
        })
})

export const isFavorited = (repoId) => new Promise((resolve, reject) => {
    createAxiosRequest('/user/favorite/' + repoId.toString(),
        {}, 'GET')
        .then(resp => {
            resolve(resp.data.favorited)
        })
        .catch(err => {
            reject(err)
        })
})

export const favoriteRepo = (repoId) => new Promise((resolve, reject) => {
    createAxiosRequest('/user/favorites',
        {favorite: repoId}, 'PUT')
        .then(resp => {
            resolve(resp.data)
        })
        .catch(err => {
            reject(err)
        })
})

export const unfavoriteRepo = (repoId) => new Promise((resolve, reject) => {
    createAxiosRequest('/user/favorites',
        {favorite: repoId}, 'DELETE')
        .then(resp => {
            resolve(resp.data)
        })
        .catch(err => {
            reject(err)
        })
})