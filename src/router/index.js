import Vue from 'vue'
import VueRouter from 'vue-router'
import RepoSearch from "@/components/RepoSearch";
import Favorites from "@/components/Favorites";
import Login from "@/components/Login"
import EventLogs from "@/components/EventLogs";
import UserList from "@/components/UserList";
import Register from "@/components/Register";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'RepoSearch',
        component: RepoSearch
    },
    {
        path: '/favorites',
        name: 'Favorites',
        component: Favorites
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/admin/event_logs',
        name: 'EventLogs',
        component: EventLogs
    },
    {
        path: '/admin/user_list',
        name: 'UserList',
        component: UserList
    }
]

const router = new VueRouter({
    mode: 'hash',
    routes
})

export default router